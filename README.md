# Drag n Drop Example
This is Unity example project with Dynamic List, Drag n Drop with Conversation and Data Serialization.
You can use it for Inventory or something like this.

**Developer: Ilya Rastorguev.**

> Please, do not use it on production!
> It's not optimal way for realization, because we need to use pool of objects instead Instanite, but it's only a serialization and drag n drop example.

## Features
- Dynamic Data object with Collapsable UI. The Interface Panel contains a dynamic list of characters that you can drag and drop on the field and they will be created.
- Characters on the field constantly generate a random goal of pursuit (a point in space), when it is reached they are looking for a new one (within the screen).
- The characters can be dragged across the field (and they will not lose their current target).
- You can clear or save data (characters, their targets, position and rotation in space) that will be initialized in the same place you saved the next time you run. Everything works on JSON files and can be encoded in Base64 if you want.

## UnityPackage Link
You can download Unity Package here:
https://gitlab.com/pipidastros/drag-n-drop-example/-/blob/unitypackage/DragDropTest.unitypackage