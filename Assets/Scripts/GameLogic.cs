﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

//=========================================================
//  This is a Small Game Project developed only for
//  portfolio reasons
//
//  @developer              Ilya Rastorguev
//  @version                1.0
//  @build                  1000
//  @url                    https://vk.com/devking
//=========================================================
//=========================================================
//  Main Game Logic Class
//=========================================================
public class GameLogic : MonoBehaviour{
    /* Singleton Instances */
    [HideInInspector] public static GameLogic Instance { get { return instance; } }   // Instance
    [HideInInspector] public static GameLogic instance = null;                        // Game Manager Instance
    [Header("Game Data Management")]
    [Tooltip("Setup Game Components Here")] public GameData gameData;                 // Game Data
    [Tooltip("Path to store Game Save")]public string gameSavePath = "/gameData.dat"; // Game Data Save Path
    [Header("Objects References")]
    public GameObject charactersListContainer;                                        // Characters List Container

    /* Current Instances */
    public GameDataInstances dataInstances;                                        // Current Instances

    #region Base Initialization
    /**
     * Before Scene Initialized
     */
    private void Awake(){
        /* Check SDK Instance. It's a realisation of Singleton */
        if (instance == null){
            instance = this;
        }else if (instance == this){
            Destroy(gameObject);
        }

        /*
         * Cant destroy this object when load another
         * scene for singleton working state
         */
        DontDestroyOnLoad(gameObject); // Don't Destroy Gamebase

        /* Setup Folders and Load Data */
        gameSavePath = Application.persistentDataPath + gameSavePath;
        loadCharactersData();
    }

    /**
     * Start is called before the first frame update
     */
    private void Start(){
        _generateCharactersList();
    }

    /*
     * Update is called once per frame
     */
    private void Update(){
        
    }
    #endregion

    #region Work with Data Serialization and Initialization
    /**
     * Load Characters Data from File
     */
    public void loadCharactersData(){
        /* Create Data Model */
        dataInstances = new GameDataInstances();
        dataInstances.items = new List<GameDataInstances.GameDataInstance>();

        /* Load Data */
        Debug.Log("Loading charatcers instances from file...");
        string _charsData = _loadFile(gameSavePath, false);
        if (_charsData != ""){
            dataInstances = JsonUtility.FromJson<GameDataInstances>(_charsData);
            Debug.Log("All Instances are Loaded. Instanite from Objects List...");
            instaniteAllObjects();
        }
    }

    /**
     * Save Characters Data
     */
    public void saveCharactersData(){
        /* Update Data for All Characters */
        for(int i = 0; i < dataInstances.items.Count; i++){
            GameDataInstances.GameDataInstance _current = dataInstances.items[i];
            CharacterController _controller = _current.link.GetComponent<CharacterController>();
            _current.pos_x = _controller.characterPosition.x;
            _current.pos_y = _controller.characterPosition.y;
            _current.rot_x = _controller.characterRotation.x;
            _current.rot_y = _controller.characterRotation.y;
            _current.target_x = _controller.characterTarget.x;
            _current.target_y = _controller.characterTarget.y;
            dataInstances.items[i] = _current;
        }

        /* Save Data */
        string _charsData = JsonUtility.ToJson(dataInstances);
        _saveFile(gameSavePath, _charsData, false);
        Debug.Log("Characters Instances Saved to File...");
    }

    /**
     * Add New Character Instance
     */
    public void addNewInstance(int index, float pos_x, float pos_y){
        /* Create Data Instance */
        GameDataInstances.GameDataInstance _instance = new GameDataInstances.GameDataInstance();
        _instance.pos_x = pos_x; _instance.pos_y = pos_y;
        _instance.rot_x = 0; _instance.rot_y = 0;
        _instance.target_x = 0; _instance.target_y = 0;
        _instance.data_item = index;
        dataInstances.items.Add(_instance);
        int _new_index = dataInstances.items.Count - 1;

        /* Create Game Object */
        GameObject _instancePrefab = GameObject.Instantiate(gameData.items[_instance.data_item].model);
        CharacterController _controller = _instancePrefab.GetComponent<CharacterController>();
        _controller.switchSkin(gameData.items[_instance.data_item].skinID);
        _controller.characterSpeed = gameData.items[_instance.data_item].speed;
        _controller.setNewPosition(_instance.pos_x, _instance.pos_y);
        _controller.setNewRotation(_instance.rot_x, _instance.rot_y);
        _controller.setNewTarget(_instance.target_x, _instance.target_y);
        _controller.currentInstanceID = _new_index;
        dataInstances.items[_new_index].link = _instancePrefab;

        /* Save Data */
        saveCharactersData();
    }

    /*
     * Clear Character Instances
     */
    public void clearAllInstances(){
        for (int i = 0; i < dataInstances.items.Count; i++){
            GameObject.Destroy(dataInstances.items[i].link);
        }

        dataInstances.items.Clear();
        saveCharactersData();
    }

    /*
     * Instanite All Objects
     */
    public void instaniteAllObjects(){
        for(int i = 0; i < dataInstances.items.Count; i++){
            /* Instanite Object */
            GameDataInstances.GameDataInstance _instance = dataInstances.items[i];
            GameObject _instancePrefab = GameObject.Instantiate(gameData.items[_instance.data_item].model);
            CharacterController _controller = _instancePrefab.GetComponent<CharacterController>();

            /* Setup Object */
            _controller.switchSkin(gameData.items[_instance.data_item].skinID);
            _controller.characterSpeed = gameData.items[_instance.data_item].speed;
            _controller.setNewPosition(_instance.pos_x, _instance.pos_y);
            _controller.setNewRotation(_instance.rot_x, _instance.rot_y);
            _controller.setNewTarget(_instance.target_x, _instance.target_y);
            _controller.currentInstanceID = i;
            dataInstances.items[i].link = _instancePrefab;
        }
        Debug.Log("All Objects are Instainted.");
    }

    /**
     * Save File
     * @param string path Path to File
     * @param string data Data to Save
     * @param bool encoded Base64 Encoded file?
     */
    protected void _saveFile(string path, string data, bool encoded = false){
        if (encoded){
            byte[] _bytesToEncode = Encoding.UTF8.GetBytes(data);
            data = Convert.ToBase64String(_bytesToEncode);
        }

        // Save File
        File.WriteAllText(path, data);
    }

    /**
     * Load File
     * @param string path Path to File
     * @param bool encoded Decode Base64 Encoded file?
     * @return string File Content
     */
    protected string _loadFile(string path, bool encoded = false){
        if (!File.Exists(path)){
            Debug.Log("Failed to load file. File " + path + " not found");
            return "";
        }

        /* Load File */
        string _data = File.ReadAllText(path);
        if (encoded){
            byte[] _decodedBytes = Convert.FromBase64String(_data);
            string _decodedData = Encoding.UTF8.GetString(_decodedBytes);
            return _decodedData;
        }

        // Return Data
        return _data;
    }
    #endregion

    #region UI Management
    /**
     * Generate Dynamic Characters list from GameData list
     */
    protected void _generateCharactersList(){
        for(int i = 0; i < gameData.items.Length; i++){
            /* Instanite Base Components */
            GameObject _element = GameObject.Instantiate(gameData.items[i].avatar, charactersListContainer.transform);
            _element.transform.Find("Image").GetComponent<Image>().sprite = gameData.items[i].avatarImage;
            _element.GetComponentInChildren<Text>().text = gameData.items[i].name;

            /* Setup Character Drop Component */
            CharacterDrop _elementComponent = _element.GetComponent<CharacterDrop>();
            _elementComponent.itemID = i;
        }
    }
    #endregion
}

//=========================================================
//  Game Data Class
//=========================================================
[System.Serializable]
public class GameData{
    /* Data Items */
    public GameDataItem[] items;

    /* Data Item Model */
    [System.Serializable]
    public class GameDataItem{
        public string name = "";    // Character Name
        public GameObject avatar;   // Charatcer Avatar Prefab
        public Sprite avatarImage;  // Character Avatar Sprite
        public GameObject model;    // Character Entity Prefab
        public int skinID = 0;      // Character Skin ID
        public float speed = 0;     // Character Speed
    }
}

//=========================================================
//  Game Data Instances Class
//=========================================================
[System.Serializable]
public class GameDataInstances{
    /* Instances List */
    public List<GameDataInstance> items;

    [System.Serializable]
    public class GameDataInstance{
        public int data_item;           // Item ID
        public float pos_x = 0;         // Position X
        public float pos_y = 0;         // Position Y
        public float rot_x = 0;         // Rotation X
        public float rot_y = 0;         // Rotation Y
        public float target_x = 0;      // Target X
        public float target_y = 0;      // Target Y
        public GameObject link;         // Link to GameObject
    }
}