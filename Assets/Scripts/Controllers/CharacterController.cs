﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//=========================================================
//  This is a Small Game Project developed only for
//  portfolio reasons
//
//  @developer              Ilya Rastorguev
//  @version                1.0
//  @build                  1000
//  @url                    https://vk.com/devking
//=========================================================
//=========================================================
//  Character Controller Component
//=========================================================
[RequireComponent(typeof(BoxCollider2D))]
public class CharacterController : MonoBehaviour{
    /* Serializable Character Fields */
    public int currentInstanceID = 0;                            // Current Instance ID
    public int currentSkin = 0;                                  // Current Skin Index
    public float characterSpeed = 5F;                            // Character Speed
    public GameObject[] skins;                                   // Character Skins
    [HideInInspector]public Vector2 characterPosition;           // Character Position
    [HideInInspector]public Quaternion characterRotation;        // Character Rotation
    [HideInInspector]public Vector2 characterTarget;             // Character Target

    /* Drag Flag */
    protected bool is_drag = false;
    
    /**
     * Initialize Character Controller
     */
    private void Awake(){
        transform.position = characterPosition;
        transform.rotation = characterRotation;
        switchSkin(currentSkin);
    }

    // Update is called once per frame
    private void Update(){
        if(!is_drag) _characterMovement();
    }

    /**
     * Set New Character Target
     * @param float x X Position
     * @param float y Y Position
     */
    public void setNewTarget(float x, float y){
        characterTarget = new Vector2(x, y);
    }

    /**
     * Set New Character Position
     */
    public void setNewPosition(float x, float y){
        transform.position = (Vector3)new Vector2(x, y);
        characterPosition = transform.position;
    }

    /**
     * Set New Character Rotation
     */
    public void setNewRotation(float x, float y){
        transform.rotation = new Quaternion(x, y, 0F, 0F);
        characterRotation = transform.rotation;
    }

    /**
     * Set New Target From Screen Coords
     * @param float x - X Position from screen
     * @param float y - Y Position from screen
     */
    public void setNewTargetFromScreen(float x, float y){
        Vector3 _tg = Camera.main.ScreenToWorldPoint(new Vector3(x,y,0F));
        setNewTarget(_tg.x, _tg.y);
    }

    /**
     * Switch Skin
     */
    public void switchSkin(int skin){
        for(int i = 0; i < skins.Length; i++){
            if(i == skin){
                currentSkin = i;
                skins[i].SetActive(true);
            }else{
                skins[i].SetActive(false);
            }
        }
    }

    /**
     * Character Movement to target
     */
    protected void _characterMovement(){
        if (Vector2.Distance(transform.position, characterTarget) > 0.1F){
            /* Move Character */
            float step = characterSpeed * Time.deltaTime;
            transform.position = Vector2.MoveTowards(transform.position, characterTarget, step);
            characterPosition = transform.position;

            /* Rotate Character */
            float angle = Mathf.Atan2(characterTarget.y - transform.position.y, characterTarget.x - transform.position.x) * Mathf.Rad2Deg;
            Quaternion targetRotation = Quaternion.Euler(new Vector3(0, 0, angle));
            transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, 90F * Time.deltaTime);
            characterRotation = transform.rotation;
        }else{
            _generateNewTarget();
        }
    }

    /**
     * Generate New Target
     */
    protected void _generateNewTarget(){
        float _randomX = Random.Range(0F, (float)Screen.width);
        float _randomY = Random.Range(0F, (float)Screen.height);
        setNewTargetFromScreen(_randomX, _randomY);
    }

    #region IDragHandler implementation
    /**
     * On Object Drag
     * @param PointerEventData eventData Event Data
     */
    public void OnMouseDrag(){
        is_drag = true;
        Vector3 pos;
        pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        setNewPosition(pos.x, pos.y);
    }

    /**
     * On Object Drop
     * @param PointerEventData data Event Data
     */
    public void OnMouseUp(){
        if (is_drag){
            is_drag = false;

        }
    }
    #endregion
}
