﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//=========================================================
//  This is a Small Game Project developed only for
//  portfolio reasons
//
//  @developer              Ilya Rastorguev
//  @version                1.0
//  @build                  1000
//  @url                    https://vk.com/devking
//=========================================================
//=========================================================
//  Collapsable Panel Component
//=========================================================
public class CollapsablePanel : MonoBehaviour{
    /* Collapsed Status */
    public bool isCollapsed = true;                 // Default Collapse State
    public string showButtonText = "ПОКАЗАТЬ";      // Show Button Text
    public string hideButtonText = "СКРЫТЬ";        // Hide

    /* Components Instances */
    protected Animator _animator;                   // Animator Component
    protected Button _button;

    // Start is called before the first frame update
    private void Start(){
        /* Get Components */
        _animator = this.gameObject.GetComponent<Animator>();
        _button = this.gameObject.transform.Find("CollapseButton").transform.gameObject.GetComponent<Button>();

        /* Initialize Animation */
        if (isCollapsed){
            CollapsePanel();
        }else{
            ShowPanel();
        }
    }

    /**
     * Collapse Panel
     */
    public void CollapsePanel(){
        isCollapsed = true;
        _animator.SetTrigger("hide");
        _button.GetComponentInChildren<Text>().text = showButtonText;
    }

    /**
     * Show Panel
     */
    public void ShowPanel(){
        isCollapsed = false;
        _animator.SetTrigger("show");
        _button.GetComponentInChildren<Text>().text = hideButtonText;
    }

    /**
     * Toggle Panel
     */
    public void TogglePanel(){
        if (isCollapsed){
            ShowPanel();
        }else{
            CollapsePanel();
        }
    }
}
