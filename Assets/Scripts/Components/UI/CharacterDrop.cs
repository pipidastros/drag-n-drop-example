﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

//=========================================================
//  This is a Small Game Project developed only for
//  portfolio reasons
//
//  @developer              Ilya Rastorguev
//  @version                1.0
//  @build                  1000
//  @url                    https://vk.com/devking
//=========================================================
//=========================================================
//  Character Drag n Drop Component
//=========================================================
[RequireComponent(typeof(RectTransform))]
public class CharacterDrop : MonoBehaviour, IDragHandler, IEndDragHandler{
    /* Link to Character */
    public int itemID = 0;

    /* Transforms Variables */
    private Transform _canvasTransform;
    private RectTransform _rectTransform;
    private GameObject _dragElement = null;

    /**
     * On Scene Started
     */
    private void Start(){
        /* Get Components */
        _rectTransform = GetComponent<RectTransform>();
        _canvasTransform = GameObject.Find("Canvas").transform;
    }

    #region IDragHandler implementation
    /**
     * On Object Drag
     * @param PointerEventData eventData Event Data
     */
    public void OnDrag(PointerEventData eventData) {
        if (_dragElement == null){
            _dragElement = Instantiate(this.gameObject, _canvasTransform);
        }else{
            Vector2 pos;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(_canvasTransform as RectTransform, Input.mousePosition, Camera.main, out pos);
            _dragElement.transform.position = _canvasTransform.TransformPoint(pos);
        }
    }

    /**
     * On Object Drop
     * @param PointerEventData data Event Data
     */
    public void OnEndDrag(PointerEventData data){
        Vector3 pos;
        pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        GameLogic.Instance.addNewInstance(itemID, pos.x, pos.y);

        /* Destroy Dropped Element */
        Destroy(_dragElement);
    }
    #endregion
}
